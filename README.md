# README #
![g12065.png](https://bitbucket.org/repo/Bqpk4B/images/3304903689-g12065.png)
## Struct ##
Estructura básica para proyecto web con MVC en PHP y GulpJs

##  Para qué sirve éste repositorio ?? ##

* Provee de una estructura básica para un proyecto web (PHP)
* Añade un conjunto de herramientas para facilitar su desarrollo

##  Cómo realizo el Set Up? ##

## 1- Instalar GULP.JS de forma global

```
#!
npm install -g gulp
```
## 2- Agregar dependencias de desarrollo al proyecto

```
#!
npm install --save-dev gulp
```
## 3- Instalar los plugins
```
#!
npm install gulp-sass gulp-concat gulp-uglify gulp-uglifycss gulp-image-optimization gulp-filesize gulp-rename --save-dev
```