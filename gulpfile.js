/**
 * Created by Martin on 07/01/2016.
 */
// Include gulp
var gulp = require('gulp');

// Include Our Plugins
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var uglifycss = require('gulp-uglifycss');
var rename = require('gulp-rename');
var imageop = require('gulp-image-optimization');
var filesize = require('gulp-filesize');

// Compilar Scss
gulp.task('scss', function() {
    return gulp.src('assets/css/scss/*.scss')
        .pipe(sass())
        .pipe(filesize())
        .pipe(gulp.dest('assets/css'));
});

// Minificar css
gulp.task('mincss', ['scss'], function () {
    gulp.src('assets/css/*.css')
        .pipe(filesize())
        .pipe(uglifycss({
            "max-line-len": 80
        }))
        .pipe(filesize())
        .pipe(gulp.dest('assets/css'));
});


// Concatena & Minifica JS
gulp.task('scripts', function() {
    return gulp.src('assets/js/**/*.js')
        .pipe(concat('all.js'))
        .pipe(gulp.dest('assets/js/js_min'))
        .pipe(rename('all.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('assets/js/js_min'));
});

// Optimiza Imágenes
gulp.task('images', function(cb) {
    gulp.src(['assets/images/png/*.png','assets/images/jpg/*.jpg','assets/images/gif/*.gif','assets/images/jpeg/*.jpeg']).pipe(imageop({
        optimizationLevel: 5,
        progressive: true,
        interlaced: true
    })).pipe(gulp.dest('assets/images/opti')).on('end', cb).on('error', cb);
});



// Observa archivos para buscar cambios
gulp.task('watch', function() {
    gulp.watch('js/*.js', ['lint', 'scripts']);
    gulp.watch('assets/css/scss/*.scss', ['scss']);
    gulp.watch('assets/css/*.css', ['mincss']);
});

// Mis Tareas
gulp.task('default', ['scss', 'mincss', 'scripts', 'images', 'watch']);
gulp.task('getcss', ['scss', 'mincss']);
gulp.task('getjs', ['scripts', 'watch']);
