<?php
/**
 *
 * @package  Estructura proyecto web
 * @author   Martín González <martingr89@hotmail.esm>
 * @link     https://es.linkedin.com/in/martingr89
 *
 */

define("SERVER","localhost");
define("USER","user");
define("PASS","pass");
define("DB","db");

//OTROS DATOS
define("DEBUG","true");

?>