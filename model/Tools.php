<?php
/**
 * Creado por Martin
 * Fecha: 07/01/2016
 * Hora: 14:07
 */
include_once '/config/functions.php';
/**
 * Clase que contiene funciones útiles sobre la funcionalidad del programa
 *
 * @package  Estructura proyecto web
 * @author   Martín González <martingr89@hotmail.esm>
 * @link     https://es.linkedin.com/in/martingr89
 *
 */

class Tools{
    /**
     * Devuelve una instancia de la conexión a la base de datos
     * @return type
     */
    function connectDB(){

        $conexion = mysqli_connect(SERVER, USER, PASS, DB);
        if($conexion){
        }else{
            echo 'Ha sucedido un error inexperado en la conexion de la base de datos<br>';
        }
        mysqli_query ($conexion,"SET NAMES 'utf8'");
        mysqli_set_charset($conexion, "utf8");
        return $conexion;
    }


    /**
     * Desconecta la base de datos a partir de la instancia que le pasamos
     * @param type $conexion
     * @return type
     */
    function disconnectDB($conexion){
        $close = mysqli_close($conexion);
        if($close){
        }else{
            echo 'Ha sucedido un error inexperado en la desconexion de la base de datos<br>';
        }
        return $close;
    }


    /**
     * Obtenemos un array multidimensional a partir de una sentencia SQL de entrada
     * @param type $sql
     * @return type
     */
    function getArraySQL($sql){
        //Creamos la conexión
        $conexion = $this->connectDB();
        //generamos la consulta
        if(!$result = mysqli_query($conexion, $sql)) die(mysqli_error($conexion));
        $rawdata = array();
        //guardamos en un array multidimensional todos los datos de la consulta
        $i=0;
        while($row = mysqli_fetch_array($result))
        {
            $rawdata[$i] = $row;
            $i++;
        }
        $this->disconnectDB($conexion);
        return $rawdata;
    }

    /**
     * Dibujamos en pantalla una tabla a partir de un array multidimensional de entrada
     * @param type $rawdata
     */
    function displayTable($rawdata){

        //DIBUJAMOS LA TABLA
        echo '<table class="table table-striped table-bordered table-condensed">';
        $columnas = count($rawdata[0])/2;
        //echo $columnas;
        $filas = count($rawdata);
        //echo "<br>".$filas."<br>";
        //Añadimos los titulos

        for($i=1;$i<count($rawdata[0]);$i=$i+2){
            next($rawdata[0]);
            echo "<th><b>".key($rawdata[0])."</b></th>";
            next($rawdata[0]);
        }
        for($i=0;$i<$filas;$i++){
            echo "<tr>";
            for($j=0;$j<$columnas;$j++){
                echo "<td>".$rawdata[$i][$j]."</td>";

            }
            echo "</tr>";
        }
        echo '</table>';
    }

    /**
     * Dibujamos en pantalla undterminado error
     *
     */
    function displayError($title,$message){

        echo'
        <div class="row">
            <div class="col-sm-4">

            </div>
            <div class="col-sm-4">
                <div class="page-header">
                    <h1>'.$title.'</h1>
                </div>
                <div class="alert alert-info">
                    '.$message.'
                </div>
            </div>
            <div class="col-sm-4">

            </div>
        </div>
        ';
    }
    /**
     *  Crea una nueva fila en la tabla dada e inserta conjunto de valores.
     * @param string $table
     * @param array $campos
     * @return type $consulta
     */
    function insert($table, $campos){
        $connect = new Tools();
        $conexion = $connect->connectDB();
        $cont = 0;
        $datos_contulta ="";

        foreach ($campos as $valor) {
            if($cont != count($campos)){
                $datos_contulta .=$valor[$cont].",";
            }
            else if($cont == count($campos)-1){
                $datos_contulta .=$valor[$cont];
            }
            $cont++;
        }
        $sql = "insert into".$table." values (".$datos_contulta.");";
        $consulta = mysqli_query($conexion,$sql);
        if($consulta){
        }else{
            echo "No se ha podido insertar en la base de datos<br><br>".mysqli_error($conexion);
        }
        $connect->disconnectDB($conexion);
        return $consulta;
    }

}
?>